package com.inkglobal.techtest;

public interface Transformable {
	String transform(String input);
}
