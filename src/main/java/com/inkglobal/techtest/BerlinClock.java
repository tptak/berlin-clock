package com.inkglobal.techtest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BerlinClock implements Transformable {

    private static final String HOURS_GROUP = "hours";

    private static final String MINUTES_GROUP = "minutes";

    private static final String SECONDS_GROUP = "seconds";

    private static final String PATTERN = String.format(
            "(?<%s>[01][0-9]|2[0-4]):(?<%s>[0-5][0-9]):(?<%s>[0-5][0-9])",
            HOURS_GROUP, MINUTES_GROUP, SECONDS_GROUP);

    private final static Pattern CLOCK = Pattern.compile(PATTERN);

    @Override
    public String transform(String input) {
        if (input == null) {
            return null;
        }

        Matcher matcher = CLOCK.matcher(input);
        if (!matcher.matches()) {
            return null;
        }

        int hours = Integer.parseInt(matcher.group(HOURS_GROUP));
        int minutes = Integer.parseInt(matcher.group(MINUTES_GROUP));
        int seconds = Integer.parseInt(matcher.group(SECONDS_GROUP));

        String hourSection = transformHourSection(hours);
        String minuteSection = transformMinutesSection(minutes);
        String secondSection = seconds % 2 == 0 ? "Y" : "O";

        return String.format("%s %s %s", secondSection, hourSection,
                minuteSection);
    }

    private String transformHourSection(int input) {
        int majorValue = input / 5;
        int minorValue = input % 5;
        return String.format("%s %s",
                transformValueToString(majorValue, 4, 'R'),
                transformValueToString(minorValue, 4, 'R'));
    }

    private String transformMinutesSection(int input) {
        int majorValue = input / 5;
        int minorValue = input % 5;
        return String.format("%s %s",
                transformValueToString(majorValue, 11, 'Y').replaceAll("YYY",
                        "YYR"),
                transformValueToString(minorValue, 4, 'Y'));
    }

    private String transformValueToString(int value, int length,
            Character positiveCharacter) {
        StringBuilder builder = new StringBuilder();

        while (length-- > 0) {
            builder.append(value-- > 0 ? positiveCharacter : 'O');
        }

        return builder.toString();
    }
}
