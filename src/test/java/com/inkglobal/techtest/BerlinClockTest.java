package com.inkglobal.techtest;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class BerlinClockTest {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] { { null, null }, { "", null },
                { "lalala", null }, { "99:99:99", null },
                { "00:00:00", "Y OOOO OOOO OOOOOOOOOOO OOOO" },
                { "13:17:01", "O RROO RRRO YYROOOOOOOO YYOO" },
                { "23:59:59", "O RRRR RRRO YYRYYRYYRYY YYYY" },
                { "24:00:00", "Y RRRR RRRR OOOOOOOOOOO OOOO" },
                { "03:34:23", "O OOOO RRRO YYRYYROOOOO YYYY" },
                { "14:52:43", "O RROO RRRR YYRYYRYYRYO YYOO" },
                { "22:33:11", "O RRRR RROO YYRYYROOOOO YYYO" }, });
    }

    private final Transformable transformer;
    private final String input;
    private final String output;

    public BerlinClockTest(String theInput, String theOutput) {
        this.transformer = new BerlinClock();
        this.input = theInput;
        this.output = theOutput;
    }

    @Test
    public void transformAndVerify() {
        Assert.assertEquals("Transformation operation returned incorrect data",
                output, transformer.transform(input));
    }

}
